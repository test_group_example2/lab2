import 'dart:io';

int m = 0,x=0;
void func1(var a) {
  m = a ~/ 2;
  for (int i = 2; i <= m; i++) {
    if (a % i == 0) {
      print('$a is not a prime number');
      x=1;
      break;
    }
  }
  if(x==0){
    print('$a is a prime number');
  }
}

void main() {
  int? num;
  print("input number : ");
  num = int.parse(stdin.readLineSync()!);
  func1(num);
}